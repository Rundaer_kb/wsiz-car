<?php

namespace App\Classes;

use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Environment;

class Mail{

    const MAIN_EMAIL = 'konradbabiarztest@gmail.com';

    protected $mailer;
    protected $twig;
    protected $appKernel;

    private $dir_mails;
    /* TODO: catch errors variable */

    public function __construct(Environment $twig, Swift_Mailer $mailer, KernelInterface $appKernel)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->appKernel = $appKernel;

        $this->dir_mails = $appKernel->getProjectDir() . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'mail' . DIRECTORY_SEPARATOR . 'templates';
    }

    public function send($title, $to, $template, $args = null)
    {
        if (!$this->scanForTemplate($template)){
            return false;
        }

        $message = (new \Swift_Message($title))
            ->setFrom(self::MAIN_EMAIL)
            ->setTo($to)
            ->addPart(
                 $this->twig->render(
                    'mail'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$template.'.html.twig',
                     $args
                ),
                'text/html'
            )
        ;

        $this->push($message);
        return true;
    }

    private function push($message){
        /* TODO: TRY / CATCH  */
        $this->mailer->send($message);
    }

    private function scanForTemplate($template)
    {
        /* TODO: Create function which can scan thorught all files and search for .html.twig */
        $files = scandir($this->dir_mails);
        $fileExists = false;
        foreach ($files as $file){
            if ($file == $template.'.html.twig'){
                $fileExists = true;
            }
        }
        return $fileExists;
    }

}