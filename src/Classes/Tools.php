<?php

namespace App\Classes;

class Tools{

    /* TODO: Display Success / Error message */


    public static function randomDate($startDate, $endDate, $count = 1 ,$dateFormat = 'Y-m-d H:i:s')
    {
        //inspired by
        // https://gist.github.com/samcrosoft/6550473

        // Convert the supplied date to timestamp
        $minDateString = strtotime($startDate);
        $maxDateString = strtotime($endDate);

        if ($minDateString > $maxDateString)
        {
            throw new Exception("From Date must be lesser than to date", 1);
        }

        for ($ctrlVarb = 1; $ctrlVarb <= $count; $ctrlVarb++)
        {
            $randomDate[] = mt_rand($minDateString, $maxDateString);
        }

        if (sizeof($randomDate) == 1)
        {
            $randomDate = date($dateFormat, $randomDate[0]);
            return $randomDate;
        } elseif (sizeof($randomDate) > 1) {
            foreach ($randomDate as $randomDateKey => $randomDateValue)
            {
                $randomDatearray[] =  date($dateFormat, $randomDateValue);
            }
            //return $randomDatearray;
            return array_values(array_unique($randomDatearray));
        }
    }

    /**
     * @param $start_date - start date
     * @param $end_date - end date
     * @param $date_from_user - given date
     * @return bool
     */
    public static function checkInRange($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
//        $start_ts = strtotime($start_date);
//        $end_ts = strtotime($end_date);
//        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($date_from_user >= $start_date) && ($date_from_user <= $end_date));
    }
}