<?php

namespace App\Controller\Account;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Controller\AccountController;
use App\Entity\Car;
use App\Entity\Journey;
use App\Form\CarFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarController extends AccountController
{
    /**
     * @Breadcrumb({
     *     { "label" = "Account", "route" = "account_index"},
     *     { "label" = "Car" },
     *     { "label" = "Add" }
     * })
     * @Security("is_granted('ROLE_USER')", statusCode=405, message="You are not logged.")
     * @Route("/account/add/car", name="account_add_car", methods={"POST", "GET"})
     *
     * @param Request $request
     *
     * @param \Swift_Mailer $mailer
     * @return Response
     */
    public function addCarAction(Request $request)
    {
        $car = new Car();
        $user = $this->getUser();

        $form = $this->createForm(CarFormType::class, $car);

        if ($request->isMethod('post')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $car->setOwner($user);

                $em = $this->getDoctrine()->getManager();
                $em->persist($car);
                $em->flush();
                $this->addFlash('success', 'Car added.');

                return $this->redirectToRoute('account_index');
            }

            $this->addFlash('error', 'Car has not been added.');
        }

        return $this->render('account/car/add.html.twig', ["form" => $form->createView()]);
    }

    /**
     * @Breadcrumb({
     *     { "label" = "Account", "route" = "account_index"},
     *     { "label" = "Car" },
     *     { "label" = "Edit" }
     * })
     * @Security("is_granted('ROLE_USER')", statusCode=405, message="You are not logged.")
     * @Route("/account/edit/car/{id}", name="account_edit_car", methods={"POST", "GET"})
     *
     * @param Request $request
     * @param Car $car
     *
     * @return Response
     */
    public function editCarAction(Request $request, Car $car)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user_journeys = $em->getRepository(Journey::class)->findBy(['owner' => $user]);

        /* If user already has some journeys active he cannot change his car */
        if(!empty($user_journeys)){
            $this->addFlash('error', 'You have active journeys, you cannot change car now.');
            return $this->redirectToRoute('account_index');
        }

        $form = $this->createForm(CarFormType::class, $car);

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $car->setOwner($user);

                $em->persist($car);
                $em->flush();

                $this->addFlash('success', 'Your car has been updated.');
                return $this->redirectToRoute('account_index');
            }
            $this->addFlash('error', 'Your car has not been updated.');
        }

        return $this->render('account/car/edit.html.twig', ["form" => $form->createView()]);
    }

    /**
     * @Security("is_granted('ROLE_USER')", statusCode=405, message="You are not logged.")
     * @Route("/account/delete/car/{id}", name="account_delete_car", methods={"GET"})
     *
     * @param Car $car
     *
     * @return RedirectResponse
     */
    public function deleteCarAction(Car $car)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user_journeys = $em->getRepository(Journey::class)->findBy(['owner' => $user]);

        /* If user already has some journeys active he cannot delete his car */
        if(!empty($user_journeys)){
            $this->addFlash('error', 'You have active journeys, first delete journeys.');
            return $this->redirectToRoute('account_index');
        }

        $em->remove($car);
        $em->flush();

        $this->addFlash("success", "Car has been deleted");

        return $this->redirectToRoute("account_index");
    }
}