<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Classes\Mail;
use App\Entity\Car;
use App\Entity\Journey;
use App\Entity\User;
use App\Form\CarFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Breadcrumb({
     *   { "label" = "Account"}
     * })
     * @Security("is_granted('ROLE_USER')", statusCode=405, message="You are not logged.")
     * @Route("/account", name="account_index")
     */
    public function index(): Response
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $car = $em->getRepository(Car::class)->findOneBy(['owner' => $user]);
        $user_journeys = $em->getRepository(Journey::class)->findBy(['owner' => $user]);
        $user_as_participant = $em->getRepository(Journey::class)->findAllByParticipant($user);

        return $this->render('account/index.html.twig', [
            'car' => $car,
            'user_journeys' => $user_journeys,
            'user_as_participant' => $user_as_participant
        ]);
    }

    /**
     * @Route("/account/delete/", name="account_delete")
     * @Security("is_granted('ROLE_USER')", statusCode=405, message="You are not logged.")
     * @param Mail $mail
     * @return RedirectResponse
     */
    public function deleteAccount(Mail $mail)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $car = $em->getRepository(Car::class)->findOneBy(['owner' => $user]);
        $user_journeys = $em->getRepository(Journey::class)->findBy(['owner' => $user]);
        $user_as_participant = $em->getRepository(Journey::class)->findAllByParticipant($user);

        /* Remove all where belong as participant */
        foreach ($user_as_participant as $journey){
            $mail->send('Joureny Canceled',$journey->getOwner()->getEmail(), 'remove_crew_member', ['user' => $user]);
            $journey->removeParticipant($user);
        }

        /* Remove all as owner */
        foreach ($user_journeys as $journey){

            foreach ($journey->getParticipants() as $participant){
                $mail->send('Joureny Canceled',$participant->getEmail(), 'delete_journey', ['owner' => $journey->getOwner()]);
            }

            $em->remove($journey);
            $em->flush();
        }
        if (!empty($car)){
            $em->remove($car);
            $em->flush();
        }

        $em->remove($user);
        $em->flush();

        $session = new Session();
        $session->invalidate();

        return $this->redirectToRoute("home_index");

    }
}
