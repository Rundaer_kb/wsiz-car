<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Classes\Mail;
use App\Entity\Journey;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Breadcrumb({
 *   { "label" = "Journey"}
 * })
 */
class JourneyController extends AbstractController
{
    /**
     * @Route("/journey", name="journey_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if (!is_null($user)){
            $journeys = $em->getRepository(Journey::class)->findAllExceptOne($user->getId());
        } else {
            $journeys = $em->getRepository(Journey::class)->findAll();
        }

        return $this->render('journey/index.html.twig', [
            "journeys" => $journeys
        ]);
    }

    /**
     * @Route ("/journey/assignuser/{id}", name="journey_assign_user")
     * @param Journey $journey
     * @param Mail $mail
     * @return RedirectResponse
     */
    public function assignUserAction(Journey $journey, Mail $mail)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $isAssigned = false;

        if(count($journey->getParticipants()) == $journey->getPersonLimit()){
            $this->addFlash('error', 'Already full');
            return $this->redirectToRoute('journey_index');
        }

        foreach ($journey->getParticipants()->getValues() as $participant){
            if ($participant->getId() === $user->getId()){
                $isAssigned = true;
            }
        }

        /* check if user is an owner or if he is already assigned */
        if ($user !== $journey->getOwner() && !$isAssigned ){

            /* check if user is already assigned to other journeys in the same time */
            $active_journeys = $em
                ->getRepository(Journey::class)
                ->findAllByParticipantAndDate($this->getUser(), $journey->getDepartureTime(), $journey->getDepartureTime());

            if (!empty($active_journeys)){
                $this->addFlash('error', 'You are either owner or participant of existing journey in this time.');
                return $this->redirectToRoute('journey_index');
            }

            $journey->addParticipant($user);

            /* check how many participans is in Journey, if full change status */
            if (count($journey->getParticipants()) == $journey->getPersonLimit()){
                $journey->setStatus(Journey::JOURNEY_STATUS_FULL);
            }

            $em->persist($journey);
            $em->flush();
            $this->addFlash('success', 'You have been added to journey.');

            $email = $journey->getOwner()->getEmail();
            $mail->send('New crew member', $email, 'new_crew_member', ['user' => $user])
                ? $this->addFlash('success', 'Owner of journey has been informed')
                : $this->addFlash('error', 'Email hasn\'t been sent');
        } else {
            $this->addFlash('error', 'You cannot join to this journey.');
        }

        return $this->redirectToRoute('journey_index');
    }

    /**
     * @Route ("/journey/removeuser/{id}", name="journey_remove_user")
     * @param Journey $journey
     * @param Mail $mail
     * @return RedirectResponse
     */
    public function removeUserAction(Journey $journey, Mail $mail)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $isAssigned = false;

        foreach ($journey->getParticipants()->getValues() as $participant){
            if ($participant->getId() === $user->getId()){
                $isAssigned = true;
            }
        }

        /* check if user is an owner or if he is already assigned */
        if ($user !== $journey->getOwner() && $isAssigned ){

            if (!empty($active_journeys)){
                $this->addFlash('error', 'You are either owner or participant of existing journey in this time.');
                return $this->redirectToRoute('journey_index');
            }

            $journey->removeParticipant($user);

            /* check how many participans is in Journey, change status */
            if (count($journey->getParticipants()) < $journey->getPersonLimit()){
                $journey->setStatus(Journey::JOURNEY_STATUS_BUILDING);
            }

            $em->persist($journey);
            $em->flush();
            $this->addFlash('success', 'You have been removed from journey.');

            $mail->send('Removed crew member',$journey->getOwner()->getEmail(), 'remove_crew_member', ['user' => $user])
                ? $this->addFlash('success', 'Owner of journey has been informed')
                : $this->addFlash('error', 'Email hasn\'t been sent');
        } else {
            $this->addFlash('error', 'You cannot being removed from this journey.');
        }

        return $this->redirectToRoute('journey_index');
    }
}
