<?php

namespace App\Controller;

use AndreaSprega\Bundle\BreadcrumbBundle\Annotation\Breadcrumb;
use App\Classes\Mail;
use App\Classes\Tools;
use App\Entity\Car;
use App\Entity\Journey;
use App\Form\ChangeJourneyStatusFormType;
use App\Form\JourneyFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Breadcrumb({
 *   { "label" = "My Journeys", "route" = "my_journey_index"}
 * })
 */
class MyJourneyController extends AbstractController
{
    /**
     * @Route("/my-journey", name="my_journey_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $journeys = $em
            ->getRepository(Journey::class)
            ->findBy(["owner" => $this->getUser()]);

        return $this->render("my_journey/index.html.twig", ["journeys" => $journeys]);
    }

    /**
     * @Breadcrumb({
     *   { "label" = "Add"}
     * })
     * @Route("/my-journey/add", name="my_journey_add", methods={"POST", "GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $journey = new Journey();
        $user = $this->getUser();
        $form = $this->createForm(JourneyFormType::class, $journey);

        /* User cannot create journey if he has no car */
        if (empty($em->getRepository(Car::class)->findOneBy(['owner' => $user]))){
            $this->addFlash("error", "You have no car .. shame.");
            return $this->redirectToRoute('my_journey_index');
        }

        if ($request->isMethod('post')) {
            $form->handleRequest($request);

            /* TODO: Change into database query */
            /* Cannot have more than one active journey in the same time period */
            $arrival = $form->get('arrivalTime')->getData();
            $departure = $form->get('departureTime')->getData();
            $user_active_journeys = $em->getRepository(Journey::class)->findBy(['owner' => $user]);
            if ($this->checkIfAnyOtherJourneyExists($user_active_journeys, $departure, $arrival)){
                $this->addFlash("error", "You cannot add journey while you already have on in this time period");
                return $this->redirectToRoute('my_journey_index');
            }

            if ($form->isValid()) {
                $journey
                    ->setOwner($user)
                    ->setPersonLimit($user->getCar()->getSeats())
                    ->setStatus(Journey::JOURNEY_STATUS_BUILDING);
                $em->persist($journey);
                $em->flush();
                $this->addFlash("success", "Journey has been added");
                return $this->redirectToRoute('my_journey_index');
            }
            $this->addFlash("error", "Journey has not been added");
        }

        return $this->render('my_journey/add.html.twig', ["form" => $form->createView()]);
    }

    /**
     * @Route("/my-journey/edit/{id}", name="my_journey_edit", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param Journey $journey
     *
     * @return Response
     */
    public function editAction(Request $request, Journey $journey)
    {
        /* if journy already has participants, you cannot edit it */
        if (count($journey->getParticipants()) > 0){
            $this->addFlash("error", "Journey has participants, you cannot edit journey while someone is signed");
            return $this->redirectToRoute('my_journey_index');
        }

        $form = $this->createForm(JourneyFormType::class, $journey);

        if ($request->isMethod('post')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($journey);
                $em->flush();

                $this->addFlash("success", "Journey has been updated");
                return $this->redirectToRoute('my_journey_index');
            }
            $this->addFlash("error", "Journey has not been updated");
        }
        return $this->render('my_journey/add.html.twig', ["form" => $form->createView()]);
    }

    /**
     * @Route("/my-journey/delete/{id}", name="my_journey_delete", methods={"GET"})
     * @param Journey $journey
     * @param Mail $mail
     * @return RedirectResponse
     */
    public function deleteAction(Journey $journey, Mail $mail)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($journey);
        $entityManager->flush();

        $this->addFlash("success", "Journey has been deleted");
        foreach ($journey->getParticipants() as $participant){

            $mail->send('Joureny Canceled',$participant->getEmail(), 'delete_journey', ['owner' => $journey->getOwner()])
                ? $this->addFlash('success', 'Email has been send to participants')
                : $this->addFlash('error', 'Email hasn\'t been sent');
        }

        return $this->redirectToRoute("my_journey_index");
    }

    /**
     * @Breadcrumb({
     *   { "label" = "Details"}
     * })
     * @Route("/my-journey/details/{id}", name="my_journey_details", methods={"GET"})
     *
     * @param Journey $journey
     *
     * @return RedirectResponse
     */
    public function detailsAction(Journey $journey)
    {
        return $this->render("my_journey/details.html.twig", [
            "journey" => $journey
        ]);
    }

    /**
     * @Route ("/my-journey/changestatus/{id}", name="my_journey_change_status")
     * @param Journey $journey
     * @param Mail $mail
     * @return RedirectResponse
     */
    public function changeStatusAction(Journey $journey, Mail $mail)
    {
        $em = $this->getDoctrine()->getManager();
        $journey->setStatus(Journey::JOURNEY_STATUS_STARTING);
        $em->persist($journey);
        $em->flush();

        $this->addFlash("success", "Journey has started");
        foreach ($journey->getParticipants() as $participant){

            $mail->send(
                'Joureny Started',
                $participant->getEmail(),
                'start_journey',
                [
                    'participant' => $participant,
                    'journey'   => $journey,
                    'owner'     => $journey->getOwner()
                ])
                ? $this->addFlash('success', 'Email has been send to participants')
                : $this->addFlash('error', 'Email hasn\'t been sent');
        }

        return $this->redirectToRoute("my_journey_index");
    }

    private function checkIfAnyOtherJourneyExists($journeys, $startDate, $endDate)
    {
        foreach ($journeys as $journey){
            return
                Tools::checkInRange($journey->getDepartureTime(), $journey->getArrivalTime(), $endDate) ||
                Tools::checkInRange($journey->getDepartureTime(), $journey->getArrivalTime(), $startDate);
        }
    }
}

