<?php

namespace App\Controller;

use App\Classes\Mail;
use App\Entity\Journey;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     * @param Mail $mail
     */
    public function index(Mail $mail)
    {


        return $this->redirectToRoute('home_index');
    }
}
