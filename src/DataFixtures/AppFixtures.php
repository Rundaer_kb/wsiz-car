<?php

namespace App\DataFixtures;

use App\Classes\Tools;
use App\Entity\Journey;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{
    private $passwordEncoder;

    private $numberOfIterate = 10;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $nameGenerator = \Nubs\RandomNameGenerator\All::create();
        $lipsumGenerator = new \joshtronic\LoremIpsum();
        $sentence = $lipsumGenerator->sentence();

        for ($i = 0; $i < (int)$this->numberOfIterate; $i++){

            /* Set new users */

            $user = new User();
            $user->setEmail("stud{$i}@wsiz.pl");
            $user->setUsername($nameGenerator->getName()); // Random name
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    '12345678'
                )
            );

            $manager->persist($user);
            $manager->flush();

            /* add couple of new journeys */
            $journey = new Journey();
            $journey
                ->setDescription($sentence)
                ->setContribution(rand(10,20))
                ->setDepartureTime(new DateTime("2020-10-01 " . rand(10,12) . ':00:00'))
                ->setArrivalTime(new DateTime("2020-10-01 " . rand(13,14) . ':00:00'))
                ->setPersonLimit(5)
                ->setStartLocation('Lancut')
                ->setFinishLocation('Rzeszow')
                ->setOwner($user);

            $manager->persist($journey);
            $manager->flush();
        }
    }
}
