<?php

namespace App\Form;

use App\Entity\Car;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'Volkswagen'    => 'volkswagen',
                    'Honda'         => 'honda',
                    'Toyota'        => 'toyota',
                ],
                'help' => 'What is your manufacturer?'
            ])
            ->add('color', ColorType::class, [
                'help' => 'Pick color of your car'
            ])
            ->add('model', ChoiceType::class, [
                'choices' => [
                    'Sedan'     => 'sedan',
                    'Hedgeback' => 'hedgeback'
                ],
                'help' => 'Choose model of your car'
            ])
            ->add('seats', NumberType::class, [
                'help' => 'Seats range is form 1-4 (regular car)',
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
