<?php

namespace App\Form;

use App\Entity\Journey;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeJourneyStatusFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    'Building'  => Journey::JOURNEY_STATUS_BUILDING,
                    'Starting'  => Journey::JOURNEY_STATUS_STARTING,
                    'Finish'    => Journey::JOURNEY_STATUS_FINISH,
                ]])
            ->add('submit', SubmitType::class, [
                    'label' => 'Change'
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Journey::class
        ]);
    }
}
