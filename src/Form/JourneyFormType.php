<?php

namespace App\Form;

use App\Entity\Journey;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JourneyFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('personLimit', HiddenType::class)
            ->add('description', TextareaType::class, [
                "label" => "Description",
                "help" => "Add short description about your journey"
            ])
            ->add('departureTime', DateTimeType::class, [
                "label" => "Departure time",
                "help" => "At what time do you leave",
                "data" => new \DateTime("now")
            ])
            ->add('arrivalTime', DateTimeType::class, [
                "label" => "Arrival time",
                "help" => "At what time do you wish to arrive",
                "data" => new \DateTime("+2 hour")
            ])
            ->add('startLocation', TextType::class, [
                "label" => "Start location",
                "help" => "Where do you start"
            ])
            ->add('finishLocation', TextType::class, [
                "label" => "Finish location",
                "help" => "Where are you heading"
            ])
            ->add('contribution', NumberType::class, [
                "label" => "Contribute",
                "help" => "Cost per one person"
            ])
            ->add('submit', SubmitType::class, ['label' => 'Send']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Journey::class,
        ]);
    }
}
