<?php

namespace App\Repository;

use App\Entity\Journey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Journey|null find($id, $lockMode = null, $lockVersion = null)
 * @method Journey|null findOneBy(array $criteria, array $orderBy = null)
 * @method Journey[]    findAll()
 * @method Journey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JourneyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Journey::class);
    }

    /**
     * Find all jourenys except one user id
     *
     * @param $id - Owner id
     *
     * @return Journey[] Returns an array of Journey objects
     */
    public function findAllExceptOne($id)
    {
        return $this->createQueryBuilder('j')
            ->leftJoin('j.owner', 'o')
            ->where('o.id != :owner_id')
            ->setParameter('owner_id', $id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllByParticipant($user)
    {
        return $this->createQueryBuilder('j')
            ->where(':user MEMBER OF j.participants')
            ->orderBy('j.departureTime', 'ASC')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllByParticipantAndDate($user, $startDate, $endDate)
    {
        return $this->createQueryBuilder('j')
            ->where(':user MEMBER OF j.participants OR j.owner = :user')
            ->andWhere(':startDate BETWEEN j.departureTime AND j.arrivalTime OR :endDate BETWEEN j.departureTime AND j.arrivalTime')
            ->setParameter('user', $user)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->getQuery()
            ->getResult()
            ;
    }
    /*
    public function findOneBySomeField($value): ?Journey
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
